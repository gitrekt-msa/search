package com.gitrekt.quora.models;

public class Question {

  private String id;
  private String userId;
  private String title;
  private String body;
  private String upvotes;
  private String subscribers;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getuserId() {
    return userId;
  }

  public void setuserId(String userId) {
    this.userId = userId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public String getUpvotes() {
    return upvotes;
  }

  public void setUpvotes(String upvotes) {
    this.upvotes = upvotes;
  }

  public String getSubscribers() {
    return subscribers;
  }

  public void setSubscribers(String subscribers) {
    this.subscribers = subscribers;
  }

  @Override
  public String toString() {
    return "Question{"
        + "id='"
        + id
        + '\''
        + ','
        + "userId='"
        + userId
        + '\''
        + ','
        + "title='"
        + title
        + '\''
        + ','
        + "body='"
        + body
        + '\''
        + ','
        + "upvotes='"
        + upvotes
        + '\''
        + ','
        + "subscribers='"
        + subscribers
        + '\''
        + ','
        + '}';
  }
}
