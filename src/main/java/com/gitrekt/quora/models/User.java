package com.gitrekt.quora.models;

public class User {

  private String id;
  private String email;
  private String username;
  private String firstName;
  private String lastName;

  public void setEmail(String email) {
    this.email = email;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public void setfirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setlastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public String getfirstName() {
    return firstName;
  }

  public String getlastName() {
    return lastName;
  }

  public String getUsername() {
    return username;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "User{"
        + "id='"
        + id
        + '\''
        + ','
        + "email='"
        + email
        + '\''
        + ','
        + "firstName='"
        + firstName
        + '\''
        + ','
        + "lastName='"
        + lastName
        + '\''
        + ','
        + "username='"
        + username
        + '\''
        + '}';
  }
}
