package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.database.postgres.PostgresConnection;
import com.gitrekt.quora.models.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

public class UserSearchUserPostgresHandler extends PostgresHandler<User> {
  public UserSearchUserPostgresHandler() {
    super("get_user_by_email", User.class);
  }

  /**
   * Returns a list of Users with an email similar to the query .
   *
   * @param param The parameter to the procedure (email)
   * @return Users with an email similar to the one in the query
   * @throws SQLException Indicates an internal server error
   */
  public List<User> getUserByEmail(Object... param) throws SQLException {

    int[] types = new int[1];
    types[0] = Types.VARCHAR;
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      ResultSet resultSet =
          super.call("SELECT * FROM get_user_by_email(?)", types, connection, param);
      List<User> users = new ArrayList<>();

      while (resultSet.next()) {
        User user = new User();
        user.setId(resultSet.getString("id"));
        user.setEmail(resultSet.getString("email"));
        user.setUsername(resultSet.getString("username"));
        user.setfirstName(resultSet.getString("first_name"));
        user.setlastName(resultSet.getString("last_name"));
        users.add(user);
      }

      return users;
    }
  }

  /**
   * Returns a list of Users with a name similar to the one in th query.
   *
   * @param param The parameter to the procedure (name)
   * @return Users with a name similar to the one in the query
   * @throws SQLException Indicates an internal server error
   */
  public List<User> getUserByName(Object... param) throws SQLException {

    int[] types = new int[1];
    types[0] = Types.VARCHAR;

    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      ResultSet resultSet =
          super.call("SELECT * FROM get_user_by_name(?)", types, connection, param);
      List<User> users = new ArrayList<>();

      while (resultSet.next()) {
        User user = new User();
        user.setId(resultSet.getString("id"));
        user.setEmail(resultSet.getString("email"));
        user.setUsername(resultSet.getString("username"));
        user.setfirstName(resultSet.getString("first_name"));
        user.setlastName(resultSet.getString("last_name"));
        users.add(user);
      }

      return users;
    }
  }

  /**
   * Returns a list of Users with a username similar to the on in the query.
   *
   * @param param The parameter to the procedure (username)
   * @return Users with a username similar to the one in the query
   * @throws SQLException Indicates an internal server error
   */
  public List<User> getUserByUsername(Object... param) throws SQLException {

    int[] types = new int[1];
    types[0] = Types.VARCHAR;
    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      ResultSet resultSet =
          super.call("SELECT * FROM get_user_by_username(?)", types, connection, param);
      List<User> users = new ArrayList<>();

      while (resultSet.next()) {
        User user = new User();
        user.setId(resultSet.getString("id"));
        user.setEmail(resultSet.getString("email"));
        user.setUsername(resultSet.getString("username"));
        user.setfirstName(resultSet.getString("first_name"));
        user.setlastName(resultSet.getString("last_name"));
        users.add(user);
      }

      return users;
    }
  }
}
