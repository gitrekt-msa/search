package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.database.postgres.PostgresConnection;
import com.gitrekt.quora.models.Question;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

public class UserSearchQuestionPostgresHandler extends PostgresHandler<Question> {
  public UserSearchQuestionPostgresHandler() {
    super("get_question_by_title", Question.class);
  }

  /**
   * Returns a list of questions with a title similar to the query.
   *
   * @param param A question title
   * @return Questions with a title similar to the one in the query
   * @throws SQLException Indicates an internal server error
   */
  public List<Question> getQuestionByTitle(Object... param) throws SQLException {

    int[] types = new int[1];
    types[0] = Types.VARCHAR;

    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      ResultSet resultSet =
          super.call("SELECT * FROM get_question_by_title(?)", types, connection, param);
      List<Question> questions = new ArrayList<>();

      while (resultSet.next()) {
        Question question = new Question();
        question.setId(resultSet.getString("id"));
        question.setuserId(resultSet.getString("user_id"));
        question.setTitle(resultSet.getString("title"));
        question.setBody(resultSet.getString("body"));
        question.setUpvotes(resultSet.getString("upvotes"));
        question.setSubscribers(resultSet.getString("subscribers"));

        questions.add(question);
      }

      return questions;
    }
  }

  /**
   * Returns a list of questions with a topic similar to the query.
   *
   * @param param A question topic
   * @return Questions with a topic similar to the one in the query
   * @throws SQLException Indicates an internal server error
   */
  public List<Question> getQuestionByTopic(Object... param) throws SQLException {
    int[] types = new int[1];
    types[0] = Types.VARCHAR;

    try (Connection connection = PostgresConnection.getInstance().getConnection()) {
      ResultSet resultSet =
          super.call("SELECT * FROM get_question_by_topic(?)", types, connection, param);
      List<Question> questions = new ArrayList<>();

      while (resultSet.next()) {
        Question question = new Question();
        question.setId(resultSet.getString("id"));
        question.setuserId(resultSet.getString("user_id"));
        question.setTitle(resultSet.getString("title"));
        question.setBody(resultSet.getString("body"));
        question.setUpvotes(resultSet.getString("upvotes"));
        question.setSubscribers(resultSet.getString("subscribers"));

        questions.add(question);
      }

      return questions;
    }
  }
}
