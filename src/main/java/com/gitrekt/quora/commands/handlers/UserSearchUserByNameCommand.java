package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.UserSearchUserPostgresHandler;
import com.gitrekt.quora.models.User;
import com.google.gson.Gson;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class UserSearchUserByNameCommand extends Command {
  private static final String[] argumentNames = new String[] {"name"};

  public UserSearchUserByNameCommand(HashMap<String, String> args) {
    super(args);
  }

  @Override
  public String execute() throws SQLException {
    checkArguments(argumentNames);

    String query = (String) args.get("name");

    UserSearchUserPostgresHandler postgresHandler = new UserSearchUserPostgresHandler();

    List<User> response = postgresHandler.getUserByName(query);
    Gson gson = new Gson();

    return gson.toJson(response);
  }
}
