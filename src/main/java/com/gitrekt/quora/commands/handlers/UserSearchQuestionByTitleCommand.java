package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.PostgresConnection;
import com.gitrekt.quora.database.postgres.handlers.UserSearchQuestionPostgresHandler;
import com.gitrekt.quora.models.Question;
import com.google.gson.Gson;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class UserSearchQuestionByTitleCommand extends Command {
  private static final String[] argumentNames = new String[] {"title"};

  public UserSearchQuestionByTitleCommand(HashMap<String, String> args) {
    super(args);
  }

  @Override
  public String execute() throws SQLException {
    checkArguments(argumentNames);

    String query = (String) args.get("title");

    UserSearchQuestionPostgresHandler postgresHandler = new UserSearchQuestionPostgresHandler();

    List<Question> response = postgresHandler.getQuestionByTitle(query);

    Gson gson = new Gson();

    return gson.toJson(response);
  }
}
