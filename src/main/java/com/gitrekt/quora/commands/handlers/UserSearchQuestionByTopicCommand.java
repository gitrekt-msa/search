package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.UserSearchQuestionPostgresHandler;
import com.gitrekt.quora.models.Question;
import com.google.gson.Gson;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class UserSearchQuestionByTopicCommand extends Command {
  private static final String[] argumentNames = new String[] {"topic"};

  public UserSearchQuestionByTopicCommand(HashMap<String, String> args) {
    super(args);
  }

  @Override
  public String execute() throws SQLException {
    checkArguments(argumentNames);

    String query = (String) args.get("topic");

    UserSearchQuestionPostgresHandler postgresHandler = new UserSearchQuestionPostgresHandler();

    List<Question> response = postgresHandler.getQuestionByTopic(query);

    Gson gson = new Gson();

    return gson.toJson(response);
  }
}
