package search_testing;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.commands.handlers.UserSearchQuestionByTitleCommand;
import com.gitrekt.quora.commands.handlers.UserSearchQuestionByTopicCommand;

import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.spy;

public class UserSearchQuestionTest {


  @Test
  public void checkQuestionExistsByTitle() {
    HashMap<String, String> args = new HashMap<>();
    args.put("title", "Who are you?");
    Command title_search_handler = spy(new UserSearchQuestionByTitleCommand(args));
    String result = "";
    try {
      result = (String) title_search_handler.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }

      assertTrue("Question exists", result.contains("Who are you?"));
  }

  @Test
  public void checkQuestionExistsByTopic() {
    HashMap<String, String> args = new HashMap<>();
    args.put("topic", "Tech");
    Command topic_search_handler = spy(new UserSearchQuestionByTopicCommand(args));
    String result = "";
    try {
      result = (String) topic_search_handler.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
    assertTrue("Question exists", result.contains("Who are you?"));
  }
}
