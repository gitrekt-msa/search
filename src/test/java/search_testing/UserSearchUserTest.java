package search_testing;

import static org.mockito.Mockito.*;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.commands.handlers.UserSearchUserByEmailCommand;

import com.gitrekt.quora.commands.handlers.UserSearchUserByNameCommand;

import com.gitrekt.quora.commands.handlers.UserSearchUserByUsernameCommand;

import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

public class UserSearchUserTest {

  @Test
  public void checkUserExistsByEmail() {
    HashMap<String, String> args = new HashMap<>();
    args.put("email", "rick@sanchez.com");
    Command email_search_handler = spy(new UserSearchUserByEmailCommand(args));
    String result = "";
    try {
      result = (String) email_search_handler.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
    assertTrue("User exists", result.contains("rick@sanchez.com"));
  }

  @Test
  public void checkUserExistsByUsername() {
    HashMap<String, String> args = new HashMap<>();
    args.put("username", "rick-sanchez");
    UserSearchUserByUsernameCommand username_search_handler =
        new UserSearchUserByUsernameCommand(args);
    String result = "";
    try {
      result = username_search_handler.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
    assertTrue("User exists", result.contains("rick-sanchez"));
  }

  @Test
  public void checkUserExistsByName() {
    HashMap<String, String> args = new HashMap<>();
    args.put("name", "Rick");
    UserSearchUserByNameCommand name_search_handler = new UserSearchUserByNameCommand(args);
    String result = "";
    try {
      result = name_search_handler.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }

    assertTrue("User exists", result.contains("Rick"));
  }

  @Test
  public void checkResponseForInvalidUserByEmail() {
    HashMap<String, String> args = new HashMap<>();
    args.put("email", "hady@hady.com");
    UserSearchUserByEmailCommand email_search_handler = new UserSearchUserByEmailCommand(args);
    String result = "";
    try {
      result = email_search_handler.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
    assertEquals("[]", result);
  }

  @Test
  public void checkResponseForInvalidUserByUsername() {
    HashMap<String, String> args = new HashMap<>();
    args.put("username", "hady1100");
    UserSearchUserByUsernameCommand username_search_handler =
        new UserSearchUserByUsernameCommand(args);
    String result = "";
    try {
      result = username_search_handler.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
    assertEquals("[]", result);
  }

  @Test
  public void checkResponseForInvalidUserByName() {
    HashMap<String, String> args = new HashMap<>();
    args.put("name", "hady");
    UserSearchUserByNameCommand name_search_handler = new UserSearchUserByNameCommand(args);
    String result = "";
    try {
      result = name_search_handler.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
    assertEquals("[]", result);
  }
}
