# Main Server

The main server is responsible for getting requests from the user,
adding the request in the queue for the respective service.

### Instructions

1. Clone the repo 

2. Run `docker-compose up -d` to start the server and all associated services

3. Access the server [http://localhost](http://localhost)